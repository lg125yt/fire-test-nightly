import asyncio
import string
import subprocess
import random
import requests
import os
import json

import aiohttp
import logging

import revolt
from revolt.ext import commands
from revolt.enums import PresenceType


global test
test = 0

class Client(commands.CommandsClient):
    async def get_prefix(self, message: revolt.Message):
      return "!"
    async def on_reaction_remove(self,message,user,emoji_id):
      pass
    async def on_reaction_add(self,message,user,emoji_id):
      pass
    async def on_ready(self):
        logging.info("Ready")
        logging.info("Logged into %s", self.user.name)
        print(f"Logged into {self.user.name}#{self.user.discriminator}")
        self.remove_command("help")
        self.add_command(Client.help)
        await self.edit_status(presence=PresenceType.focus,text="Do !help | https://firefork.ct8.pl/")
        keepalive()
  
    @commands.command()
    async def test(self, ctx: commands.Context):
        print(ctx)
        print(f"""author: {ctx.author}
              client: {ctx.client.all_commands}
              command: {ctx.command}
              args: {ctx.args}
              message: {ctx.message.content}
              state: {ctx.state.api_info}
              invoker: {ctx.invoked_with}
              server: {ctx.server_id}
              view: {ctx.view.value}""")
        global test
        test += 1
        await ctx.message.reply(f"""Test command complete, now what the fuck do i do...
        debug info sent to console, also this command has been used {test} times this session!""")
        print(ctx.author.dm_channel)
        print(ctx.state.servers)
        print(ctx.state.api_info)
  
    @commands.command()
    async def help(self, ctx: commands.Context):
      await ctx.message.reply("This is FireTest, FireFork's testing bot! Features on FireFork are not on this bot, and only upcoming features will be on here for testing.")

          
async def main():
    async with aiohttp.ClientSession() as session:
        token = os.environ['token']
        client = Client(session, token,api_url="https://nightly.haydar.dev/api/")
        await client.start()

asyncio.run(main())
